# Ardit Meti - Challenge

## :computer: How to execute

1. Build the maven project payment-service ```mvn clean install```

2. I configured and added the payment-service to the docker-compose file so just running:
```docker-compose up``` would be enough.

After that the kafka consumer inside the payments-service will start and you can see logs at the url: ```http://localhost:9000/logs```

## :memo: Notes

There is a KafkaConsumer which will listen and process the payments.
For the online payments there is an HttpService file which does the http POST requests to verify the payments and to store error logs if expections happen.

## :pushpin: Things to improve

I would improve the error handling with proper Exceptions, to better catch the error types and write some tests.
