package com.arditmeti.tt.paymentservice.model;

public enum ErrorType {
    DATABASE("database"), NETWORK("network"), OTHER("other");

    private final String name;

    ErrorType(String name) {
        this.name = name;
    }

    public String getValue() {
        return this.name;
    }
}
