package com.arditmeti.tt.paymentservice.model;

public class Error {

    private int paymentId;
    private String error;
    private String errorDescription;

    public Error(int paymentId, String error, String errorDescription) {
        this.paymentId = paymentId;
        this.error = error;
        this.errorDescription = errorDescription;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
