package com.arditmeti.tt.paymentservice.service;

import com.arditmeti.tt.paymentservice.model.ErrorType;
import com.arditmeti.tt.paymentservice.model.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class HttpService {

    private final RestTemplate restTemplate;

    @Value("${payments.post.url}")
    private String paymentUrl;

    @Value("${logs.post.url}")
    private String logsUrl;

    public HttpService(RestTemplateBuilder restTemplate) {
        this.restTemplate = restTemplate.build();
    }

    public ResponseEntity<String> postNewPayment(Payment payment) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        Map<String, Object> map = new HashMap<>();
        map.put("payment_id", payment.getPaymentId());
        map.put("account_id", payment.getAccountId().getAccountId());
        map.put("payment_type", payment.getPaymentType());
        map.put("credit_card", payment.getCreditCard());
        map.put("amount", payment.getAmount());

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        return restTemplate.postForEntity(paymentUrl, entity, String.class);
    }

    public void postNewErrorLog(String paymentId, ErrorType errorType, String errorDesc) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        Map<String, Object> map = new HashMap<>();
        map.put("payment_id", paymentId);
        map.put("error_type", errorType.getValue());
        map.put("error_description", errorDesc);

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        restTemplate.postForEntity(logsUrl, entity, String.class);
    }
}
