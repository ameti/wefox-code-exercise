package com.arditmeti.tt.paymentservice.kafka;

import com.arditmeti.tt.paymentservice.model.Account;
import com.arditmeti.tt.paymentservice.model.ErrorType;
import com.arditmeti.tt.paymentservice.model.Payment;
import com.arditmeti.tt.paymentservice.service.AccountService;
import com.arditmeti.tt.paymentservice.service.HttpService;
import com.arditmeti.tt.paymentservice.service.PaymentService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Transactional
public class KafkaConsumer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);
    private final PaymentService paymentService;
    private final AccountService accountService;
    private final HttpService httpService;

    public KafkaConsumer(AccountService accountService, PaymentService paymentService, HttpService httpService) {
        this.accountService = accountService;
        this.paymentService = paymentService;
        this.httpService = httpService;
    }

    @KafkaListener(topics = "offline", groupId = "payments")
    public void processOfflinePayment(String paymentJSON){
        logger.info("received offline payment = '{}'", paymentJSON);
        ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Payment paymentEntity = null;
        try{
            paymentEntity = mapper.readValue(paymentJSON, Payment.class);
            executeQueries(paymentEntity);
        } catch (Exception e){
            logger.error("An error occurred! '{}'", e.getMessage());
            assert paymentEntity != null;
            httpService.postNewErrorLog(paymentEntity.getPaymentId(), ErrorType.OTHER, e.getMessage() + paymentJSON);
        }
    }

    @KafkaListener(topics = "online", groupId = "payments")
    public void processOnlinePayment(String paymentJSON){
        logger.info("received online payment = '{}'", paymentJSON);
        ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Payment paymentEntity = null;
        try{
            paymentEntity = mapper.readValue(paymentJSON, Payment.class);
            ResponseEntity<String> thirdPartyRes = httpService.postNewPayment(paymentEntity);
            if(thirdPartyRes.getStatusCode() == HttpStatus.OK) {
                executeQueries(paymentEntity);
            }
        } catch (Exception e){
            logger.error("An error occurred! '{}'", e.getMessage());
            assert paymentEntity != null;
            httpService.postNewErrorLog(paymentEntity.getPaymentId(), ErrorType.OTHER, e.getMessage() + paymentJSON);
        }
    }

    private void executeQueries(Payment paymentEntity) {
        try {
            Account account = accountService.getAccountByAccountId(paymentEntity.getAccountId().getAccountId());
            paymentEntity.setAccountId(account);
            Payment payment = paymentService.addPayment(paymentEntity);
            account.setLastPaymentDate(payment.getCreatedOn());
            accountService.updateAccount(account);
            logger.info("Success process payment '{}' with topic '{}'", paymentEntity.getPaymentId(), "offline");
        } catch (Exception e) {
            logger.error("An database error occurred! '{}'", e.getMessage());
            httpService.postNewErrorLog(paymentEntity.getPaymentId(), ErrorType.DATABASE, e.getMessage());
        }
    }
}
