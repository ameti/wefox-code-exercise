package com.arditmeti.tt.paymentservice.service;

import com.arditmeti.tt.paymentservice.model.Account;
import com.arditmeti.tt.paymentservice.repo.AccountRepo;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private final AccountRepo accountRepo;

    public AccountService(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    public void updateAccount(Account account) {
        accountRepo.save(account);
    }

    public Account getAccountByAccountId(int id) {
       return accountRepo.getAccountByAccountId(id);
    }
}
