package com.arditmeti.tt.paymentservice.repo;

import com.arditmeti.tt.paymentservice.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepo extends JpaRepository<Account, Integer> {
    Account getAccountByAccountId(int id);
}
