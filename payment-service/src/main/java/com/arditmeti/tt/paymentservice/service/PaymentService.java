package com.arditmeti.tt.paymentservice.service;

import com.arditmeti.tt.paymentservice.model.Payment;
import com.arditmeti.tt.paymentservice.repo.PaymentRepo;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {

    private final PaymentRepo paymentRepo;

    public PaymentService(PaymentRepo repo) {
        this.paymentRepo = repo;
    }

    public Payment addPayment(Payment payment) {
        return paymentRepo.save(payment);
    }
}
