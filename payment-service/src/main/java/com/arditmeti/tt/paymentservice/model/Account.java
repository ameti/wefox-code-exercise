package com.arditmeti.tt.paymentservice.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "accounts")
public class Account {
    @Id
    @Column(name = "account_id", nullable = false)
    private int accountId;

    public Account() { }
    public Account(int id) { accountId = id; }

    private String name;

    private String email;

    private Date birthday;

    @Column(name = "last_payment_date")
    private Timestamp lastPaymentDate;

    @Column(name = "created_on")
    private Timestamp createdOn;



    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Timestamp getLastPaymentDate() {
        return lastPaymentDate;
    }

    public void setLastPaymentDate(Timestamp lastPaymentDate) {
        this.lastPaymentDate = lastPaymentDate;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }
}
